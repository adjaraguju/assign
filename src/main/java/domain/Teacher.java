package domain;

public class Teacher extends User {
    public Teacher(){}

    public Teacher(String name, String surname, String username, String passwordStr){
        super(name,surname,username,passwordStr);
    }

    protected Teacher(int id, String name, String surname, String username, String passwordStr){
        super(id,name,surname,username,passwordStr);
    }

}
