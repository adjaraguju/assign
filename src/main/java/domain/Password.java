package domain;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String passwordStr;

    public Password(String passwordStr){
        setPasswordStr(passwordStr);
    }

    public String getPasswordStr(){
        return passwordStr;
    }

    private void setPasswordStr(String passwordStr) {
        if(checkPasswordStr(passwordStr)){
            this.passwordStr = passwordStr;
        }
        else{
            System.out.println("Password.setPasswordStr: Incorrect password format : " + passwordStr );
        }
    }

    private static boolean checkPasswordStr(String passwordStr){
        boolean isLower = false, isUpper = false, isDigit = false;
        if(passwordStr.length()>9) {
            for (int i = 0; i < passwordStr.length(); i++) {
                if (Character.isDigit(passwordStr.charAt(i))) {
                    isDigit = true;
                } else if (Character.isLetter(passwordStr.charAt(i))) {
                    if (Character.isLowerCase(passwordStr.charAt(i))) isLower = true;
                    else isUpper = true;
                }
            }
        }
        return (isLower && isUpper && isDigit);
    }

    @Override
    public String toString() {
        return passwordStr;
    }
}
