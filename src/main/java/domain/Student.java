package domain;

import java.util.ArrayList;
import java.util.HashMap;

public class Student extends User {
    private double gpa;
    private HashMap<Subject,ArrayList<Integer>> marks;

    public Student(){
        fill();
    }

    public Student(String name, String surname, String username, String passwordStr){
        super(name,surname,username,passwordStr);
        fill();
    }

    protected Student(int id, String name, String surname, String username, String passwordStr){
        super(id,name,surname,username,passwordStr);
        fill();
    }

    public ArrayList<Integer> getMarks(){
        return marks.get(Subject.MATH);

    }

    public void printMarks(){
        for(Subject subject : Subject.values()){
            System.out.println(subject.getSubject() + " : " + marks.get(subject));
        }
    }



    public double getGpa(){
        return calculateGpa();
    }

    public double calculateGpa(){
        double GPA = 0;
        double current;
        for(Subject subject : Subject.values()){
            current = 0;
            for(int i = 0;i<marks.get(subject).size();i++){
                current += marks.get(subject).get(i);
            }
            GPA += current/(marks.get(subject).size());
        }
        return GPA/4;
    }
    private void fill(){
        marks = new HashMap<Subject,ArrayList<Integer>>();
        for(Subject subject : Subject.values()){
            ArrayList<Integer> temp = new ArrayList<>();
            marks.put(subject, temp);
        }
    }

    public void setMarks(Subject subject, int mark){
        ArrayList<Integer> list= marks.get(subject);
        list.add(mark);
        marks.put(subject,list);
    }



}
