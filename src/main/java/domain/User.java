package domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class User {
    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password
    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;
    private String username;
    private Password password;
    ArrayList<User> friends;

    private void readId()  {
        try{
            File file = new File("C:\\Users\\acer\\Desktop\\db.txt");
            Scanner sc = new Scanner(file);
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                String[] data = line.split(" ");
                id_gen = Integer.parseInt(data[0]);
            }
            ++id_gen;
        }
        catch(Exception e){
            System.out.println("User.readId : File not found" + e);
        }

    }

    private void idGen() {
        readId();
        id = id_gen++;
    }

    public User(){
        idGen();
    }

    public User(String username, String passwordStr){
        this();
        setUsername(username);
        setPassword(new Password(passwordStr));
    }
    // For Sign Up
    public User(String name, String surname, String username, String passwordStr){
        this();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(new Password(passwordStr));
    }
    // Constructor for exist Users
    protected User(int id, String name, String surname, String username, String passwordStr){
        this.id = id;
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(new Password(passwordStr));
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getSurname(){
        return surname;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password.getPasswordStr();
    }

    public void setName(String name){
        this.name = name;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(Password password){
        this.password = password;
    }

    public void addFriend(User friend){
        friends.add(friend);
    }

    public void friendsList(){
        for(User friend : friends){
            System.out.println(getFriend(friend));
        }
    }

    public String getFriend(User friend){
        return friend.getName() + " " + friend.getSurname();
    }

    @Override
    public String toString(){
        return id + " " + name  + " " + surname + " " + username + " " + password.toString();
    }

}
