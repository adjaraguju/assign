package domain;

public enum Subject {
    MATH("Math"),PHYSICS("Physics"),BIOLOGY("Biology"),CHEMISTRY("Chemistry");

    String subject;

    Subject(String subject){
        setSubject(subject);
    }

    private void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject(){
        return subject;
    }

}
