package domain;

import org.w3c.dom.ls.LSOutput;

import java.awt.print.PrinterGraphics;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class MyApplication  {
    ArrayList<Student> students;
    ArrayList<Teacher> teachers;


    private Scanner sc = new Scanner(System.in);
    private Student signedStudent;
    private Teacher signedTeacher;

    public MyApplication(){
        students = new ArrayList<>();
        teachers = new ArrayList<>();
    }

    private void addStudent(Student student) {
        students.add(student);
    }

    public void start() throws IOException {                //STARTTTTTTTTTTTTTTTTTTTTTT
        readFile();
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

    }

    private void readFile() throws FileNotFoundException {
        File fileStudent = new File("C:\\Users\\acer\\IdeaProjects\\Assign\\src\\main\\java\\domain\\student");
        File fileTeacher = new File("C:\\Users\\acer\\IdeaProjects\\Assign\\src\\main\\java\\domain\\teacher");
        Scanner fileScanner = new Scanner(fileStudent);
        Scanner teacherSc = new Scanner(fileTeacher);
        while(fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            String[] data = line.split(" ");
            Student clone = new Student(Integer.parseInt(data[0]),data[1],data[2],data[3], data[4]);
            students.add(clone);
        }
        while(teacherSc.hasNextLine()){
            String line = teacherSc.nextLine();
            String[] data = line.split(" ");
            Teacher teacher = new Teacher(Integer.parseInt(data[0]),data[1],data[2],data[3], data[4]);
            teachers.add(teacher);
        }
    }

    private void menu() throws IOException {
        while (true) {
            if (signedStudent == null && signedTeacher==null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else return;
            }
            else {
                userProfile();
            }
        }
    }

    private void authentication() throws IOException {
        // sign in
        // sign up
        while(true){
            System.out.println("1. Sign in");
            System.out.println("2. Sign up");
            System.out.println("3. Back");
            int choice = sc.nextInt();
            if(choice == 1) signIn();
            else if(choice == 2) signUp();
            else if(choice == 3) menu();
        }
    }

    private void signIn() throws IOException {
        while (true){
            System.out.println("Enter your username: ");
            String username = sc.next();
            System.out.println("Enter your password:");
            String passwordStr = sc.next();
            if(checkUser(username, passwordStr)){
                signedStudent = getSignedStudent(username,passwordStr);
                signedTeacher = getSignedTeacher(username,passwordStr);
                userProfile();
            }
            else{
                System.out.println("MyApplication.signIn: Incorrect username or password!!!");
            }
        }
    }

    private void signUp() throws IOException {
        while(true) {
            System.out.println("Enter your name: ");
            String name = sc.next();
            System.out.println("Enter your surname: ");
            String surname = sc.next();
            System.out.println("Enter your username: ");
            String username = sc.next();
            if (checkUsername(username)) {
                System.out.println("Enter your password: ");
                String passwordStr = sc.next();
                try {
                    signedStudent = new Student(name, surname, username, passwordStr);
                } catch (Exception i){
                    break;
                }
                break;
            } else {
                System.out.println("This username has already taken!!!");
            }
        }
        addStudent(signedStudent);
        userProfile();

    }

    private void userProfile() throws IOException {
        if(signedTeacher!=null) {
            while (true) {
                System.out.println(signedTeacher.getName() + " " + signedTeacher.getSurname());
                System.out.println("Status: online");
                System.out.println("1. Menu");
                int choice = sc.nextInt();
                if (choice == 1) {
                    teacherMenuList();
                }
            }
        }
        else{
            while (true) {
                readMarks();
                System.out.println(signedStudent.getName() + " " + signedStudent.getSurname());
                System.out.println("Status: online");
                System.out.println("1. Menu");
                int choice = sc.nextInt();
                if (choice == 1) {
                    studentMenuList();
                }
            }
        }
    }

    public void teacherMenuList() throws IOException {
        while(true){
            System.out.println("1. Students");
            System.out.println("2. Log off");
            System.out.println("3. Back");
            int choice = sc.nextInt();
            if(choice == 1){
                studentList();
            }
            else if(choice == 2){
                logOff();
            }
            else if(choice == 3){
                userProfile();
            }
        }
    }

    public void studentList() throws IOException {
        while(true){
            int back;
            int i = 1;
            for(Student student : students){
                System.out.println(i + ". " + student.getName() + " " + student.getSurname());
                i++;
            }
            back = i;
            System.out.println(back + ". Back");
            int choice = sc.nextInt();
            i = 1;
            for(Student student : students){
                if(i==choice){
             //       putMarks(student);
                    student.printMarks();
                    System.out.println("1. Set mark");
                    System.out.println("2. Back");
                    int choose = sc.nextInt();
                    if(choose == 1) {
                        setMark(student);
                    }
                    else if(choose == 2){
                        studentList();
                    }
                }
                i++;
            }
            if(choice == back){
                teacherMenuList();
            }
        }
    }

    public void putMarks(Student student) throws FileNotFoundException {
        int i = 1;
        for(Student st : students){
            if(st == student){break;}
            i++;
        }
        File file = new File("C:\\Users\\acer\\IdeaProjects\\Assign\\src\\main\\java\\domain\\marks");
        Scanner fileSc = new Scanner(file);
        while (fileSc.hasNextLine() && i!=i*4){

            String line = fileSc.nextLine();
            String[] data = line.split(" ");
            if((i*4-4)==i){

            }

        }

    }

    public void studentMenuList() throws IOException {
        while(true) {
            System.out.println("1. Marks");
            System.out.println("2. Log off");
            System.out.println("3. Back");
            int choice = sc.nextInt();
            if(choice == 1 ){
                marks();
            }
            else if(choice == 2) {
                logOff();
            }
            else if(choice == 3){
                userProfile();
            }
        }
    }

    public void setMark(Student student) throws IOException {
        while(true) {
            int i = 1;
            System.out.println("Choose a subject");
            for (Subject subject : Subject.values()) {
                System.out.println(i + ". " + subject.getSubject());
                i++;
            }
            System.out.println(i + ". Back");
            while (true) {
                int choice = sc.nextInt();
                if(choice == i){
                    teacherMenuList();
                }
                i = 1;
                for (Subject subject : Subject.values()) {
                    if (choice == i) {
                        System.out.println("Set a mark between 2 and 5:");
                        int mark = sc.nextInt();
                        if (mark >= 2 && mark <= 5) {
                            student.setMarks(subject, mark);
                            System.out.println("Successfully added");
                            System.out.println("1. Back");
                            int choose = sc.nextInt();
                            if(choose == 1){studentList();}
                        }
                        else {
                            System.out.println("Wrong mark");
                            setMark(student);
                        }
                    }
                    i++;
                }
            }

        }
    }

    public void readMarks() throws FileNotFoundException {
        File markFile = new File("C:\\Users\\acer\\IdeaProjects\\Assign\\src\\main\\java\\domain\\marks");
        Scanner markSc = new Scanner(markFile);
        for(Subject subject : Subject.values()) {
            while (markSc.hasNextLine()) {

                String line = markSc.nextLine();
                String[] data = line.split(" ");
                // User clone = new User(Integer.parseInt(data[0]),data[1],data[2],data[3], data[4]);
                for (int i = 0; i < data.length; i++) {
                    signedStudent.setMarks(subject, Integer.parseInt(data[i]));
                }
                break;
            }
        }
    }
    public void marks() throws IOException {
        signedStudent.printMarks();
        while(true) {
            System.out.println("Your GPA: " + signedStudent.getGpa());
            System.out.println("1. Back");
            int choice = sc.nextInt();
            if (choice == 1) {
                studentMenuList();
            }
        }
    }

    private void logOff() throws IOException {
        signedTeacher = null;
        signedStudent = null;
        saveUserList();
        menu();
    }

    // Checking for existing username
    private boolean checkUsername(String usernameStr){
        for(Student user : students){
            if(user.getUsername().equals(usernameStr)){
                return false;
            }
        }
        for(Teacher user : teachers){
            if(user.getUsername().equals(usernameStr)){
                return false;
            }
        }

        return true;
    }

    // Check for a valid username and password for User
    private boolean checkUser(String username, String password){
        for(Student user : students){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return true;
            }
        }
        for(Teacher user : teachers){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return true;
            }
        }
        return false;
    }

    private Student getSignedStudent(String username, String password){
        for(Student user : students){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }

    private Teacher getSignedTeacher(String username, String password){
        for(Teacher user : teachers){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }


    private void saveUserList() throws IOException {
        String content = "";
        for(Student student : students){
            content += student + "\n";
        }
        Files.write(Paths.get("C:\\Users\\acer\\IdeaProjects\\Assign\\src\\main\\java\\domain\\student"), content.getBytes());
        content = "";
        for(Teacher teacher : teachers) {
            content += teacher + "\n";
        }
        Files.write(Paths.get("C:\\Users\\acer\\IdeaProjects\\Assign\\src\\main\\java\\domain\\teacher"), content.getBytes());
// f
    }


}
